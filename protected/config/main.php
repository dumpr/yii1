<?php

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web Application',
	'preload'=>array('log'),
	'sourceLanguage'=>'en',
	'language'=>'ru',
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'qweqwe',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),

	'components'=>array(

		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName'=>false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

		'db'=>require(dirname(__FILE__).'/database.php'),

        'clientScript' => array(
            'packages' => array(),
            'scriptMap' => array(
				'jquery.js'=>false,
				'jquery.min.js'=>false,
            ),
            'coreScriptPosition'=>CClientScript::POS_END, // POS_HEAD POS_READY POS_END
            'defaultScriptPosition'=>CClientScript::POS_END,
            'defaultScriptFilePosition'=>CClientScript::POS_END
        ),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			// 'errorAction'=>'site/error',
		),

		'cache'=>array(
			'class'=>'system.caching.CDummyCache',
		),

		'format'=>array(
			'class'=>'application.extensions.timeago.TimeagoFormatter',
			'locale'=>'ru',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

		'image'=>array(
			'class'=>'ext.imageapi.CImage',
			'presets'=>array(
				'gonta'=>array(
					'cacheIn'=>'webroot.assets.gonta',
					'template'=>'application.views.site.fspresets.gonta',// /site/fspresets/hunta
					'actions'=>array(
						'scaleAndCrop'=>array(
							'width'=>640,
							'height'=>480
						),
						/*'watermark'=>array(
							'pngWatermark'=>'webroot.images.copyright.png',
							'x'=>0,
							'y'=>0
						),*/
					),
				),
				'hunta'=>array(
					'cacheIn'=>'webroot.assets.hunta',
					'template'=>'application.views.site.fspresets.hunta',
					'actions'=>array(
						'scaleAndCrop'=>array(
							'width'=>100,
							'height'=>75
						),
						/*'watermark'=>array(
							'pngWatermark'=>'/images/copyright.png',
							'x'=>50,
							'y'=>25
						),*/
					),
				),
				'punta'=>array(
					'cacheIn'=>'webroot.assets.punta',
					'template'=>'application.views.site.fspresets.punta',
					'actions'=>array(
						'scaleAndCrop'=>array(
							'width'=>40,
							'height'=>30
						)
					),
				),
				'osla'=>array(
					'cacheIn'=>'webroot.assets.osla',
					'template'=>'application.views.site.fspresets.osla',
					'actions'=>array(
						'scaleAndCrop'=>array(
							'width'=>27,
							'height'=>20
						)
					),
				),
			),
		),

		'filestore' => array(
			'class'=>'ext.filestore.FileStore',
			'downloadPath'=>'site/getfile/',
			'dummy_ico' => '/images/dummy_file.png',
			'allowedImgExts'=>array(
					'jpeg',
					'jpg',
					'png',
					'gif'
				),
			'allowedDocExts'=>array(
					'doc',
					'docx',
					'xls',
					'xlsx',
					'pdf',
					'txt',
					'zip',
					'rar',
					'7z',
					'djvu',
					'aac'
				),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);
