<?php

class File extends CActiveRecord
{

	public function tableName()
	{
		return '{{file}}';
	}

	/**
	 *
	 */
	public function rules()
	{
		return array(
			array('src', 'required'),
			array('m, ext', 'length', 'max'=>8),
			array('m_id, order', 'numerical', 'integerOnly'=>true),
			array('name, src', 'length', 'max'=>127),
			array('description', 'length', 'max'=>255),
			array('id, m, m_id, src, name, description, info, time, is_image', 'safe', 'on'=>'search'),
		);
	}


	/*public function getThump()
	{
		if(empty($this->is_image))
			return $this->src;

		return str_replace('.'.$this->ext, '_thumb.'.$this->ext, $this->src);
	}
	public function getOriginal()
	{
		if(empty($this->is_image))
			return $this->src;

		return str_replace('.'.$this->ext, '_original.'.$this->ext, $this->src);
	}*/

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 *
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'm' => 'M',
			'm_id' => 'M_id',
			'src' => 'Src',
			'name' => 'Name',
			'description' => 'Description',
			'info' => 'Info',
			'time' => 'Time',
		);
	}

	/**
	 *
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('m',$this->m,true);
		$criteria->compare('m_id',$this->m_id,true);
		$criteria->compare('src',$this->src,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('info',$this->info,true);
		$criteria->compare('time',$this->time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 *
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
