<?php

/**
 * This is the model class for table "{{file_more}}".
 *
 * The followings are the available columns in table '{{file_more}}':
 * @property string $id
 * @property string $lat
 * @property string $lon
 * @property string $make
 * @property string $model
 * @property integer $created
 * @property integer $height
 * @property integer $width
 *
 * The followings are the available model relations:
 * @property File $id0
 */
class FileMore extends CActiveRecord
{
	public function tableName()
	{
		return '{{file_more}}';
	}

	public function rules()
	{
		return array(
			array('id', 'required'),
			array('created, height, width', 'numerical', 'integerOnly'=>true),
			array('id', 'length', 'max'=>11),
			array('lat, lon', 'length', 'max'=>17),
			array('make, model', 'length', 'max'=>255),
			array('id, lat, lon, make, model, created, height, width', 'safe'),
		);
	}

	public function relations()
	{
		return array(
			'id0' => array(self::BELONGS_TO, 'File', 'id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'lat' => 'Lat',
			'lon' => 'Lon',
			'make' => 'Make',
			'model' => 'Model',
			'created' => 'Created',
			'height' => 'Height',
			'width' => 'Width',
		);
	}

	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('lat',$this->lat,true);
		$criteria->compare('lon',$this->lon,true);
		$criteria->compare('make',$this->make,true);
		$criteria->compare('model',$this->model,true);
		$criteria->compare('created',$this->created);
		$criteria->compare('height',$this->height);
		$criteria->compare('width',$this->width);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
