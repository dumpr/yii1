<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	public function actionTestFile()
	{

		$model = new stdClass;
		$model->id = 15;
/*
		// картинки
		$file = File::model()->find('m="message" AND m_id="'.$model->id.'"');
		// $file = "/images/hohoho.jpg";
		$thumb = Yii::app()->image->createUrl('gonta',$file);
		if ($thumb)
			$img = '<img src="'.$thumb.'" alt="'.$file->description.'"/>';

		// файл на скачку
		$file = File::model()->find('m="blog" AND m_id="'.$model->id.'"');
		if (empty($file->is_image))
			$link = '<a href="/site/getfile/'.$file->src.'">'.$file->description.'</a>';
*/
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/filedrop-min.js');
        Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.dragsort-0.5.2.min.js');

        $aix = implode('","',Yii::app()->filestore->allowedImgExts);
        $adx = implode('","',Yii::app()->filestore->allowedDocExts);
        $js = <<<EOT
    var allowedImgExts = ["{$aix}"];
    var allowedDocExts = ["{$adx}"];
    var allowedExts = allowedImgExts.concat(allowedDocExts);
	$(document).ready(function(){
		fileUploader('{$this->id}','{$model->id}','site/fsUploader?preset=hunta'); // ?preset=hunta
	});
EOT;
/*
	icoLoader('{$this->id}','{$model->id}');
	$('#{$this->id}Form_{$model->id}').on('click','.attache_holder .fa-times',function(){
	    if(confirm('Удалить прикреплённый файл?')){
	        var parDiv = $(this).parent();
	        $.post('admin/{$this->id}/attacheDelete',{src:$(parDiv).data('fileSrc'),id:"{$model->id}"});
	        $(parDiv).remove();
	    }
	});
	$('#{$this->id}Form_{$model->id} .attache_holder').dragsort({
		dragSelector: "div.ext",
		dragSelectorExclude: "input,textarea,select"
	});
*/

		Yii::app()->clientScript->registerScript("attache_uploader".$model->id, $js);

		$this->render('test_file',array(
				'model'=>$model,
				'link'=>isset($link) ? $link : '',
				'img'=>isset($img) ? $img : ''));
	}


	/**
	 *
	 */
	public function actionFsUploader()
	{
		if(isset($_SERVER['HTTP_X_FILE_NAME'])){
			$store = Yii::app()->filestore->store();
			if(!$store)
				Yii::app()->end();

			// далее не обязательно
			$data['id'] = Yii::app()->filestore->id;
			$data['name'] = Yii::app()->filestore->basename;
			if(empty(Yii::app()->filestore->isImage))
				$data['src'] = Yii::app()->filestore->downloadLink(Yii::app()->filestore->newPath);

			if(isset($_GET['preset']) && isset(Yii::app()->image->presets[$_GET['preset']]['template'])){
				if(!empty(Yii::app()->filestore->isImage))
					$data['src'] = Yii::app()->image->createUrl($_GET['preset'],array(
								'src'=>Yii::app()->filestore->newPath,
								'ext'=>Yii::app()->filestore->extension
							));
				$data['html'] = $this->renderPartial(Yii::app()->image->presets[$_GET['preset']]['template'],array('data'=>$data),true);
			}

			header('Content-Type: application/json');
			echo CJSON::encode($data);
		}
		Yii::app()->end();
	}


	/**
	 *
	 */
	public function actionGetFile()
	{
		$hash = explode('getfile/',Yii::app()->request->pathInfo);
		if(isset($hash[1]))
			$file = File::model()->find('src="'.$hash[1].'"');
		if(empty($file))
			throw new CHttpException(404,'The requested page does not exist.');

		$filename = Yii::getPathOfAlias('application.data').'/'.$file->src;
		if(file_exists($filename)){
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename="'.$file->name.'.'.$file->ext.'"');
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: '.$file->size);
			//ob_clean();
			flush();
			readfile(Yii::getPathOfAlias('application.data').'/'.$file->src);
			Yii::app()->end();
		} else throw new CHttpException(404,'The requested page does not exist.');

	}

	/**
	 *
	 */
	public function actionIndex()
	{
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}