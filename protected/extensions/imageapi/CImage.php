<?php
class CImage extends CApplicationComponent
{
	public $toolkit;

	public $presets=array();

	public $exif;

	public function init()
	{
		parent::init();

		Yii::import('ext.imageapi.ImageToolkit');
		Yii::import('ext.imageapi.GDImageToolkit');
		$this->toolkit = new GDImageToolkit;
	}

	public function getInfo($file)
	{
		return $this->toolkit->getInfo($file);
	}

	public function createPath($presetName, $file, $ext='')
	{
		$file_original = str_replace(array('\\', '/'), DIRECTORY_SEPARATOR, $file);

		if (!file_exists($file_original)) return false;

		if (isset($this->presets[$presetName])) {
			$preset = $this->presets[$presetName];
			$file = str_replace(array('\\', '/'), DIRECTORY_SEPARATOR, $file);
			$f = explode('protected'.DIRECTORY_SEPARATOR.'data',$file);
			$targetPath = Yii::getPathOfAlias($preset['cacheIn']);
			$targetPath = str_replace(array('\\', '/'), DIRECTORY_SEPARATOR, $targetPath);
			if(isset($f[1]))
				$targetPath .= substr($f[1], 0, 6);
			$basename = basename($file);
			$targetFile = $targetPath . DIRECTORY_SEPARATOR . $basename;
			if($ext)
				$targetFile .= '.'.$ext;

			if (!file_exists($targetPath))
				mkdir($targetPath, 0777, true); // mkdir recursive

			if (!file_exists($targetFile)) {
				copy($file_original, $targetFile);

				$this->exif_orient($targetFile);
				foreach($preset['actions'] as $action=>$params) {
					switch($action) {
					case 'scaleAndCrop':
						$this->scaleAndCrop(
								$targetFile,
								$targetFile,
								$params['width'],
								$params['height']);
						break;
					case 'scale':
						$this->scale(
								$targetFile,
								$targetFile,
								$params['width'],
								$params['height']);
						break;
					case 'resize':
						$this->resize(
								$targetFile,
								$targetFile,
								$params['width'],
								$params['height']);
						break;
					case 'rotate':
						$this->rotate(
								$targetFile,
								$targetFile,
								$params['degrees'],
								$params['background']);
						break;
					case 'crop':
						$this->crop(
								$targetFile,
								$targetFile,
								$params['x'],
								$params['y'],
								$params['width'],
								$params['height']);
						break;
					}
				}
			}
			return $targetFile;
		} else
			return false;
	}

	/**
	 *
	 */
	public function createUrl($presetName, $file)
	{
		$ext = '';
		if(is_array($file)){
			$ext = $file['ext'];
			$file = $file['src'];
		}
		elseif(is_numeric($file)){
			$model = File::model()->findByPk($file);
			list($file,$ext) = $this->fileFromModel($model->attributes);
		}
		elseif(is_object($file)){
			list($file,$ext) = $this->fileFromModel($model->attributes);
		} else {
			$arr = explode('.', $file);
			if(isset($arr[1]))
				$ext = end($arr);
		}

		if (!file_exists($file)) return false;

		if (isset($this->presets[$presetName])) {
			$preset = $this->presets[$presetName];
			$targetPath = Yii::getPathOfAlias($preset['cacheIn']);
			$generatedPath = $this->createPath($presetName, $file, $ext);

			$webrootPath = Yii::getPathOfAlias('webroot');
			if(strpos($generatedPath, $webrootPath) !== FALSE) {
				$generatedPath = substr($generatedPath, strlen($webrootPath));
			}
			$generatedPath = str_replace('\\', '/', $generatedPath);
			$home = str_replace('\\', '/', $_SERVER['DOCUMENT_ROOT']);
			$generatedUrl = str_replace($home, '', $generatedPath);
			//qw(Yii::app()->request->getBaseUrl(true),'e');
			return $generatedUrl;
		} else
			return false;
	}

	public function fileFromModel($model)
	{
		return array(Yii::getPathOfAlias('application.data').'/'.$model['src'],'.'.$model['ext']);
	}

	public function createAbsoluteUrl($presetName, $file)
	{
		return Yii::app()->getRequest()->getHostInfo().$this->createUrl($presetName, $file);
	}

	public function scaleAndCrop($source, $destination, $width, $height)
	{
		$info = self::getInfo($source);

		$scale = max($width / $info['width'], $height / $info['height']);
		$x = round(($info['width'] * $scale - $width) / 2);
		$y = round(($info['height'] * $scale - $height) / 2);

		if ($this->toolkit->resize($source, $destination, $info['width'] * $scale, $info['height'] * $scale)) {
			return $this->toolkit->crop($destination, $destination, $x, $y, $width, $height);
		}
		return false;
	}

	public function scale($source, $destination, $width, $height)
	{
		$info = self::getInfo($source);

		// Don't scale up.
		if ($width >= $info['width'] && $height >= $info['height']) {
			return false;
		}

		$aspect = $info['height'] / $info['width'];
		if ($aspect < $height / $width) {
			$width = (int)min($width, $info['width']);
			$height = (int)round($width * $aspect);
		} else {
			$height = (int)min($height, $info['height']);
			$width = (int)round($height / $aspect);
		}

		return $this->toolkit->resize($source, $destination, $width, $height);
	}

	public function resize($source, $destination, $width, $height)
	{
		return $this->toolkit->resize($source, $destination, $width, $height);
	}

	public function rotate($source, $destination, $degrees, $background = 0x000000)
	{
		return $this->toolkit->rotate($source, $destination, $degrees, $background);
	}

	public function crop($source, $destination, $x, $y, $width, $height)
	{
		return $this->toolkit->crop($source, $destination, $x, $y, $width, $height);
	}

	public function getExifInfo($file)
	{
		$info = self::getInfo($file);
		if($info['extension'] == "tiff" || $info['extension'] == "jpg" || $info['extension'] == "jpeg")
		{
			$this->exif = @exif_read_data($file, 0, true);
			return $this->exif;
		}
	}

	public function exif_orient($file)
	{
		$this->getExifInfo($file);
		if(empty($this->exif) || !isset($this->exif['IFD0']['Orientation']))
			return false;

		$ort = $this->exif['IFD0']['Orientation'];
		switch($ort)
	    {
			case 2: // horizontal flip
				//$this->image->flip(EasyImage::FLIP_HORIZONTAL);
				break;
			case 3: // 180 rotate left
				//$this->image->rotate('180');
				$this->rotate($file,$file,'180');
				break;
			case 4: // vertical flip
				//$this->image->flip(EasyImage::FLIP_VERTICAL);
				break;
			case 5: // vertical flip + 90 rotate right
				//$this->image->flip(EasyImage::FLIP_VERTICAL);
				//$this->image->rotate('90');
				break;
			case 6: // 90 rotate right
				//$this->image->rotate('90');
				$this->rotate($file,$file,'-90');
				break;
			case 7: // horizontal flip + 90 rotate right
				//$this->image->flip(EasyImage::FLIP_HORIZONTAL);
				//$this->image->rotate('90');
				break;
			case 8: // 90 rotate left
				//$this->image->rotate('-90');
				$this->rotate($file,$file,'90');
				break;
		}
	}
}