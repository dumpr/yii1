<?php

class Fm extends CApplicationComponent
{

	private $hash;
	private $upload_basename;
	private $upload_extension;
	private $upload_filename;
	private $tempFilePath;
	private $tempThumbPath;
	private $tempOriginalPath;
	private $data;
	private $temp_path;
	public $isImage;
	public $image;
	public $exif;

	public $dummy_ico;
	public $allowedExts;
	public $allowedImgExts;
	public $allowedDocExts;

	/*public function init()
	{
		parent::init();
	}*/

	/**
	 *
	 */
	public function isAllowedByExt()
	{
        return in_array($this->upload_extension, array_merge($this->allowedImageExts,$this->allowedFileExts));
	}


	/**
	 *
	 */
	public function setTempFilePath()
	{
		//if(empty($this->tempFilePath))
			//$this->tempFilePath = $this->temp_path . $this->hash . '.' . $this->upload_extension;
        return $this->tempFilePath = $this->temp_path . $this->hash . '.' . $this->upload_extension;
	}


	/**
	 *
	 */
	public function setTempThumbPath()
	{
		if(empty($this->tempThumbPath))
			$this->tempThumbPath = $this->temp_path . $this->hash . '_thumb.' . $this->upload_extension;
        return $this->tempThumbPath;
	}


	/**
	 *
	 */
	public function setTempOriginalPath()
	{
		if(empty($this->tempOriginalPath))
			$this->tempOriginalPath = $this->temp_path . $this->hash . '_original.' . $this->upload_extension;
        return $this->tempOriginalPath;
	}


	/**
	 * http://php.net/manual/ru/function.exif-imagetype.php#113253
	 */
	public function is_jpeg()
	{
		if(empty($this->data)) $this->data = file_get_contents($this->tempFilePath);
		return (bin2hex($this->data[0]) == 'ff' && bin2hex($this->data[1]) == 'd8');
	}


	/**
	 *
	 */
	public function is_png()
	{
		if(empty($this->data)) $this->data = file_get_contents($this->tempFilePath);
		return (bin2hex($this->data[0]) == '89' && $this->data[1] == 'P' && $this->data[2] == 'N' && $this->data[3] == 'G');
	}


	/**
	 *
	 */
	public function isImage()
	{
		/*if($this->is_jpeg() || $this->is_png()){
			$this->isImage = true;
		}*/
		if(empty($this->isImage))
			$this->isImage = getimagesize($this->setTempFilePath());
		return $this->isImage;
	}


	/**
	 *
	 */
	public function getOriginName()
	{
		return $this->upload_basename;
	}


	/**
	 *
	 */
	public function getExt()
	{
		return $this->upload_extension;
	}

	/*
	 * http://php.net/manual/en/function.pathinfo.php#107461
	 * Use this function in place of pathinfo to make it work with UTF-8 encoded file names too
	 */
	public function utfPathinfo($filepath)
	{
	    preg_match('%^(.*?)[\\\\/]*(([^/\\\\]*?)(\.([^\.\\\\/]+?)|))[\\\\/\.]*$%im',$filepath,$m);
	    // if($m[1]) $ret['dirname']=$m[1];
	    if($m[2]) $this->upload_basename = $m[2]; // имя файла с раширением
	    if($m[5]) $this->upload_extension = strtolower($m[5]); // расширение
	    if($m[3]) $this->upload_filename = $m[3]; // имя файла
	}


	/**
	 *
	 */
	public function returnInfo()
	{
		return json_encode(array(
			'src' => $this->tempFilePath,
			'ext' => $this->upload_extension,
			'name' => $this->upload_filename,
			'hash' => $this->hash,
			));
	}


	/**
	 *
	 */
	public function Load()
	{
		$filepath = urldecode(@$_SERVER['HTTP_X_FILE_NAME']);
		$this->utfPathinfo($filepath);
		if($this->isAllowedByExt()) {
			$this->data = file_get_contents("php://input");
			file_put_contents($this->setTempFilePath(),$this->data);
			if($this->isImage()){
				$this->image = new EasyImage($this->tempFilePath);
				//$this->exif_orient();
				return array($this->tempFilePath,$this->image);
			}
			return array($this->tempFilePath,false);
		}
	}


	/**
	 *
	 */
	public static function store($tempPath)
	{

		if(empty($tempPath))
			return false;

		$tempPath = self::absolutePath($tempPath);
		$pathinfo = pathinfo($tempPath);
		sscanf($pathinfo['basename'], "%2s%2s%s", $one, $two, $three);
		$newPath = '/data/'.$one.'/'.$two;
		// if(!file_exists($newPath))
			@mkdir($_SERVER['DOCUMENT_ROOT'].$newPath,0777,true);
		if(rename($tempPath, self::absolutePath($newPath.'/'.$three)))
			return $newPath.'/'.$three;
	}


	/**
	 *
	 */
	public static function delete($path)
	{
		if(empty($path))
			return false;

		$fn = self::absolutePath($path);
		if(file_exists($fn) && unlink($fn))
			return true;
	}


	/**
	 *
	 */
	public static function checkAsset($path)
	{
		if(strrpos($path,'ssets'))
			return self::store($path);
		return $path;
	}


	/**
	 *
	 */
	public static function absolutePath($path)
	{
		return $_SERVER['DOCUMENT_ROOT'].'/'.ltrim(str_replace($_SERVER['DOCUMENT_ROOT'], '', $path),'/');
	}


	/**
	 *
	 */
	public static function relativePath($path)
	{
		return str_replace($_SERVER['DOCUMENT_ROOT'], '', $path);
	}


	/**
	 *
	 */
	function __construct($file = false)
	{
		$this->tempFilePath = self::absolutePath($file);
		// if($file) $this->data = file_get_contents(self::absolutePath($file));
		$this->temp_path = $_SERVER['DOCUMENT_ROOT'].'/assets/temp/';
		$this->hash = md5(microtime());
	}


	/**
	 *
	 */
	public function getExifInfo()
	{
		//exif only supports jpg in our supported file types
		if($this->upload_extension == "jpg" || $this->upload_extension == "jpeg")
		{
			if(empty($this->exif))
				$this->exif = @exif_read_data($this->tempFilePath);
			return $this->exif;
		}
	}


	/**
	 *
	 */
	public function exif_orient()
	{
		$this->getExifInfo();
		if(empty($this->exif) || !isset($this->exif['Orientation']))
			return false;

		$ort = $this->exif['Orientation'];
		switch($ort)
	    {
	        case 2: // horizontal flip
	            $this->image->flip(EasyImage::FLIP_HORIZONTAL);
	        	break;
	        case 3: // 180 rotate left
	        	$this->image->rotate('180');
	        	break;
	        case 4: // vertical flip
	            $this->image->flip(EasyImage::FLIP_VERTICAL);
	       		break;
	        case 5: // vertical flip + 90 rotate right
	            $this->image->flip(EasyImage::FLIP_VERTICAL);
	        	$this->image->rotate('90');
	        	break;
	        case 6: // 90 rotate right
	        	$this->image->rotate('90');
	        	break;
	        case 7: // horizontal flip + 90 rotate right
	            $this->image->flip(EasyImage::FLIP_HORIZONTAL);
	        	$this->image->rotate('90');
	        	break;
	        case 8: // 90 rotate left
	        	$this->image->rotate('-90');
	        	break;
	    }
	}


	/**
	 *
	 */
    public function smart_resize($save_path, $width, $height, $sq = false)
    {
    	if(empty($this->isImage))
    		return false;

        if($this->isImage[0] < $width) {
        	$width = $this->isImage[0];
        }
        if($this->isImage[1] < $height) {
        	$height = $this->isImage[1];
        }

		/*$percentage = ( $width >= $height ) ? 100 / $width * $dimension : 100 / $height * $dimension;

		$newWidth = $width / 100 * $percentage;
		$newHeight = $height / 100 * $percentage;
		$image->resize($newWidth, $newHeight);*/

        if($this->isImage[0]/$this->isImage[1] > $width/$height) {
        	$this->image->resize($width, $height, EasyImage::RESIZE_WIDTH);
        }
        else {
        	$this->image->resize($width, $height, EasyImage::RESIZE_HEIGHT);
        }

		if($sq)
			$this->image->crop($width, $height);
		$this->image->save($save_path,80);

        return $this;
    }

}