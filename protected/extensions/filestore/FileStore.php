<?php

class FileStore extends CApplicationComponent
{

	public $downloadPath='site/getfile/';

	public $dummy_ico;
	public $allowedExts;
	public $allowedImgExts;
	public $allowedDocExts;
	public $newPath;
	public $id;
	public $basename;
	public $extension;
	public $filename;
	public $isImage;
	public $exif;

	private $data;
	private $hash;

	/*public function init()
	{
		parent::init();
	}*/

	public function isAllowedByExt()
	{
        return in_array(strtolower($this->extension), array_merge($this->allowedImgExts,$this->allowedDocExts));
	}

	/*
	 * http://php.net/manual/en/function.pathinfo.php#107461
	 * Use this function in place of pathinfo to make it work with UTF-8 encoded file names too
	 */
	public function utfPathinfo($filepath)
	{
		preg_match('%^(.*?)[\\\\/]*(([^/\\\\]*?)(\.([^\.\\\\/]+?)|))[\\\\/\.]*$%im',$filepath,$m);
		// if($m[1]) $ret['dirname']=$m[1];
		if($m[2]) $this->basename = $m[2]; // имя файла с раширением
		if($m[5]) $this->extension = strtolower($m[5]); // расширение
		if($m[3]) $this->filename = $m[3]; // имя файла
	}

	public function downloadLink($src)
	{
		return rtrim($this->downloadPath,'/').'/'.(strlen($src)==34 ? $src : substr($src,-34));
	}

	public function store()
	{
		$filepath = urldecode(@$_SERVER['HTTP_X_FILE_NAME']);
		$this->utfPathinfo($filepath);
		if(!$this->isAllowedByExt())
			return false;

		$this->data = file_get_contents("php://input");
		$mtime = microtime();
		$this->hash = md5($mtime);
		$this->isImage = (in_array($this->extension,$this->allowedImgExts) && substr($_SERVER['HTTP_X_FILE_TYPE'], 0, 6) == "image/") ? 1 : 0;
		sscanf($this->hash, "%2s%2s%s", $one, $two, $three);
		$newPath = Yii::getPathOfAlias('application.data').'/'.$one.'/'.$two;
		@mkdir($newPath,0777,true);

		if(file_put_contents($newPath.'/'.$three,$this->data)){

			$file = new File;
			$file->src = $one.'/'.$two.'/'.$three;
			$file->name = $this->filename;
			$file->ext = $this->extension;
			$file->size = filesize($newPath.'/'.$three);
			$file->is_image = $this->isImage;
			$mt = explode(" ", $mtime);
			$file->time = $mt[1] + $mt[0];
			if(!$file->save())
				qw($file->errors,'l');
			//return $newPath.'/'.$three;
			$this->id = $file->id;
			if($this->exif = @exif_read_data($newPath.'/'.$three,0,true))
				$this->collectExifInfo();
			$this->newPath = $newPath.'/'.$three;
			return $file->id;
		}
	}

	public function collectExifInfo()
	{
		if(isset($this->exif['GPS']['GPSLatitude']))
			list($info['lat'],$info['lon']) = $this->getGpsData();
		if(isset($this->exif['IFD0']['Make']))
			$info['make'] = trim($this->exif['IFD0']['Make']);
		if(isset($this->exif['IFD0']['Make']))
			$info['model'] = trim($this->exif['IFD0']['Model']);
		if(isset($this->exif['IFD0']['DateTime']))
			$info['created'] = strtotime($this->exif['IFD0']['DateTime']);
		/*if(isset($this->exif['COMPUTED']['Height']))
			$info['height'] = $this->exif['COMPUTED']['Height'];
		if(isset($this->exif['COMPUTED']['Width']))
			$info['width'] = $this->exif['COMPUTED']['Width'];*/
		if(!isset($info))
			return false;

		$more = new FileMore;
		$more->attributes = $info;
		$more->id = $this->id;
		if(!$more->save())
			qw($more->errors,'l');
	}

	public function getGpsData()
	{
		$latitude = $this->gps($this->exif['GPS']['GPSLatitude'], $this->exif['GPS']['GPSLatitudeRef']);
		$longitude = $this->gps($this->exif['GPS']['GPSLongitude'], $this->exif['GPS']['GPSLongitudeRef']);

		return array($latitude,$longitude);
	}


	public function gps($coordinate, $hemisphere) {
		for ($i = 0; $i < 3; $i++) {
			$part = explode('/', $coordinate[$i]);
			if (count($part) == 1) {
				$coordinate[$i] = $part[0];
			} else if (count($part) == 2) {
				$coordinate[$i] = floatval($part[0])/floatval($part[1]);
			} else {
				$coordinate[$i] = 0;
			}
		}
		list($degrees, $minutes, $seconds) = $coordinate;
		$sign = ($hemisphere == 'W' || $hemisphere == 'S') ? -1 : 1;
		return $sign * ($degrees + $minutes/60 + $seconds/3600);
	}

	public function delete($path)
	{
		$file = File::model()->find('src="'.$path.'"');
		if($file){
			$fn = Yii::getPathOfAlias('application.data').'/'.$file->src;
			if(file_exists($fn) && unlink($fn))
				return true;
		}
	}

}