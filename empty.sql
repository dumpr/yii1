-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `tbl_file`;
CREATE TABLE `tbl_file` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `m` varchar(8) NOT NULL,
  `m_id` int(11) unsigned NOT NULL,
  `src` varchar(127) NOT NULL,
  `name` varchar(127) NOT NULL,
  `ext` varchar(8) NOT NULL,
  `size` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `is_image` tinyint(1) NOT NULL,
  `order` int(11) NOT NULL,
  `time` decimal(15,4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `album` (`m`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `tbl_file_more`;
CREATE TABLE `tbl_file_more` (
  `id` int(11) unsigned NOT NULL,
  `lat` decimal(12,8) NOT NULL,
  `lon` decimal(12,8) NOT NULL,
  `make` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `created` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  KEY `id` (`id`),
  CONSTRAINT `tbl_file_more_ibfk_1` FOREIGN KEY (`id`) REFERENCES `tbl_file` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2015-01-03 09:32:31
