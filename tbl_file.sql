-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `tbl_file`;
CREATE TABLE `tbl_file` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `m` varchar(8) NOT NULL,
  `m_id` int(11) unsigned NOT NULL,
  `src` varchar(127) NOT NULL,
  `name` varchar(127) NOT NULL,
  `ext` varchar(8) NOT NULL,
  `size` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `is_image` tinyint(1) NOT NULL,
  `order` int(11) NOT NULL,
  `time` decimal(15,4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `album` (`m`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2014-12-30 12:59:47
