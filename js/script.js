function fileUploader(m,id,url){
    // site 15 site/fsUploader
    var file_ext;
    if(url==null) url = 'admin/'+m+'/fileUploader';
    var parent = $("#"+m+"Form_"+id);
    var holder = $(parent).find(".attache_holder");
    $('<div class="attache_loader"></div>')
        .appendTo(parent)
        .filedrop({multiple: true})
        .on('fdsend', function (e, files) {
            $.each(files, function (i, file) {
                file_ext = file.name.split(".").slice(-1).toString().toLowerCase();
                if($.inArray(file_ext,allowedExts) == -1){
                    $('<div class="mess-thumb"><img src="images/dummy_file.png"><b>'+file.name+'</b> Загрузка файлов <b>.'+file_ext+'</b> не поддерживается.</div>').prependTo(holder);
                } else {
                    $('<div class="mess-thumb" data-up-file-name="'+file.name+'"><i class="fa fa-spinner fa-spin"></i><b>'+file.name+'</b> загружается <a href="" onClick="$(this).parent().remove();return false">Отменить</a></div>').prependTo(holder);
                    file.sendTo(url);
                }
            })
        })
        .on('filedone', function (e, file, xhr) {
                var response = jQuery.parseJSON(xhr.responseText);
                $(holder).find('[data-up-file-name="'+response.name+'"]').remove();
                $(response.html).prependTo(holder).on('click','.fa-times',function(){
                    $(this).parent().remove();
                });
        });
}